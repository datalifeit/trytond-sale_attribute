# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, DictSchemaMixin, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval

__all__ = ['SaleAttributeSet', 'SaleAttribute',
    'SaleAttributeAttributeSet', 'Sale']


class SaleAttributeSet(ModelSQL, ModelView):
    "Sale Attribute Set"
    __name__ = 'sale.attribute.set'

    name = fields.Char('Name', required=True, translate=True,
        help="The main identifier of sale attribute set.")
    attributes = fields.Many2Many('sale.attribute-sale.attribute-set',
        'attribute_set', 'attribute', 'Attributes',
        help="Add attributes to the set.")


class SaleAttribute(DictSchemaMixin, ModelSQL, ModelView):
    "Sale Attribute"
    __name__ = 'sale.attribute'

    sets = fields.Many2Many('sale.attribute-sale.attribute-set',
        'attribute', 'attribute_set', 'Sets',
        help="Add sets to the attribute.")


class SaleAttributeAttributeSet(ModelSQL):
    "Sale Attribute - Set"
    __name__ = 'sale.attribute-sale.attribute-set'

    attribute = fields.Many2One('sale.attribute', 'Attribute',
        ondelete='CASCADE', select=True, required=True)
    attribute_set = fields.Many2One('sale.attribute.set', 'Set',
        ondelete='CASCADE', select=True, required=True)


class Sale:
    __metaclass__ = PoolMeta
    __name__ = 'sale.sale'

    attributes = fields.Dict('sale.attribute', 'Attributes',
        domain=[
            ('sets', '=', Eval('attribute_set', -1)),
            ],
        states={
            'readonly': ~Eval('attribute_set'),
            },
        depends=['attribute_set'],
        help="Add attributes to the variant.")
    attribute_set = fields.Many2One('sale.attribute.set', 'Attribute Set',
        help="Select a set of attributes to apply on the variants.")

    @fields.depends('attribute_set', 'attributes')
    def on_change_attribute_set(self):
        if self.attribute_set:
            self.attributes = {a.name: None
                for a in self.attribute_set.attributes}
