datalife_sale_attribute
=======================

The sale_attribute module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_attribute/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_attribute)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
